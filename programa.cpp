#include <iostream>
#include "Proceso.h"

using namespace std;

int main (int argc, char *argv[]) {

    // Comprobar que solo se entregue un argumento
    if (argc != 2) {
        cout << "Error, se debe pasar 1 argumento (link)" << endl;
        return 1;
    }
    else{
        // Instanciar clase proceso y pasarle link correspondiente
        Proceso descargador(argv[1]);
        descargador.crear();
        descargador.descargarAudio();
    }
   
    return 0;
}
