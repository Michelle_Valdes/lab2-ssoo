#include <iostream>

using namespace std;

#ifndef PROCESO_H
#define PROCESO_H

class Proceso {

    public:
        Proceso();
        Proceso(char* link);

        void crear();
        int descargarAudio();

    private:
        char* link;
        pid_t pid;
};
#endif