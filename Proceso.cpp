#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include "Proceso.h"

using namespace std;

Proceso::Proceso() {}

Proceso::Proceso(char* link) {
    this->link = link;
}

void Proceso::crear() {
    pid = fork();
}

int Proceso::descargarAudio() {

    if (pid < 0) {
        cout << "No se ha podido crear proceso hijo" << endl;
        return -1;
    }
    else if (pid == 0) {
        cout << "Descargando audio con proceso hijo" << endl;
        cout << "PID actual: " << getpid() << endl;

        //char* param[] = {"youtube-dl", link, "-x", "-o", "musica.mp3"};

        execlp("youtube-dl", "youtube-dl", link, "-x", "-o", "musica.mp3", NULL);

        cout << "Descarga terminada" << endl;
    }
    else{
        wait(NULL);
        cout << "Pasando a programa padre" << endl;
        cout << "PID actual: " << getpid() << endl;

        execlp ("mpv", "mpv", "musica.opus", NULL);
    }
    return 0;
}